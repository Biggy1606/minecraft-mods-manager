import { Grid } from "@material-ui/core";
import curseforge from "mc-curseforge-api";
import { useEffect, useState } from "react";

const ModList = ({ data }: { data: any }): JSX.Element => {

    const [modsArray, setModsArray] = useState<JSX.Element[]>()
    const printElements = () => {
        let temp: JSX.Element[] = []
        data.map((dataChunk: Mod, index: any) => {
            temp.push(
                <Grid item key={index} style={{height: "100px"}}>
                    <a href={dataChunk.url} style={{margin: 0, padding: 0, display: "inline-block", height: "100px"}}>
                        <img src={dataChunk.logo.url} style={{ maxHeight: "100px", maxWidth: "100px" }}></img>
                    </a>
                </Grid>)
        })
        setModsArray(temp)

    }

    useEffect(() => {
        printElements()
    }, [data])


    return (
        <Grid container justify="center">

            {/* {JSON.stringify(data)} */}
            {modsArray}

        </Grid>)
}

export default ModList;