import { Button, TextField } from "@material-ui/core";
import curseforge from "mc-curseforge-api";
import { useState } from "react";
import ModList from "./ModList";

function App() {
  const [modsList, setModsList] = useState<Mod[] | Error | ModFile[]>([]);
  const [pageIndex, setPageIndex] = useState<number>(1);

  const downloadModsList = (pageIndex: number) => {
    curseforge.getMods({ gameVersion: "1.16.5", index: pageIndex}).then(response => {
      console.log(response);

      setModsList(response)
    })
  }

  const downloadModsFileList = () => {
    curseforge.getModFiles(225643).then(response => {
      setModsList(response)
    })
  }

  const changePageIndexHandle = (event: any) => {
    setPageIndex(event.target.value);
  }

  return (
    <div>
      <h1>mc-curseforge-api test</h1>
      <TextField id="standard-basic" label="page" onChange={changePageIndexHandle} value={pageIndex}/>
      <Button onClick={() => downloadModsList(pageIndex)}>DOWNLOAD LIST</Button>
      <Button onClick={downloadModsFileList}>DOWNLOAD MOD FILES</Button>
      <div>
        <ModList data={modsList} />
      </div>
    </div>
  );
}

export default App;
