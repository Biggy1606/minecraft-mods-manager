<center>

<img src="logo.svg" title="Logo" width="200">

## Info
Proxy project forked from: https://github.com/jesperorb/node-api-proxy

> [Official **Node API Proxy** README](PROXY.md)

***

</center>

### My changes

- added https server
- added script generating rsa certificates
- setted up for `addons-ecs.forgesvc.net` curseforge API